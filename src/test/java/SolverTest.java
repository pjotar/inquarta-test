import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class SolverTest {

    @Test
    public void test() {
        Solver solver = new Solver("jdbc:postgresql://localhost:5433/inquarta-test", "postgres", "postgres", 10);

        assertThat(solver.solve(), is(55));
    }

    @Test
    public void testTransform() {
        Solver.transform();
        //проверяю вручную
    }


}
