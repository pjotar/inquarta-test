public class Main {

    // jdbc:postgresql://localhost:5433/inquarta-test postgres postgres 10
    public static void main(String[] args) {

        if (args.length != 4)
            throw new IllegalArgumentException("Укажите параметры jdbcurl login password, N");

        Solver solver = new Solver();
        solver.setJdbcurl(args[0]);
        solver.setLogin(args[1]);
        solver.setPassword(args[2]);
        solver.setN(Integer.parseInt(args[3]));

        System.out.println(solver.solve());

    }
}
