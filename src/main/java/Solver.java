import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Solver {

    private String jdbcurl, login, password;

    private Integer n;

    static {
        try {
            Class.forName(org.postgresql.Driver.class.getName());
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public Solver() {
    }

    public Solver(String jdbcurl, String login, String password, Integer n) {
        this.jdbcurl = jdbcurl;
        this.login = login;
        this.password = password;
        this.n = n;
    }

    public int solve() {
        try {
            clearDatabase();
            insertRecords();
            createXml();
            transform();
            return parseAndSumm();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    Connection getConnection() throws SQLException {
        return DriverManager.getConnection(jdbcurl, login, password);
    }

    void clearDatabase() throws SQLException {
        try (Connection connection = getConnection()) {
            Statement statement = connection.createStatement();
            statement.executeUpdate("TRUNCATE TABLE \"TEST\"");
        }
    }

    void insertRecords() throws SQLException {
        try (Connection connection = getConnection()) {
            Statement statement = connection.createStatement();
            statement.executeUpdate("INSERT INTO \"TEST\" (\"FIELD\")\n" +
                    "  SELECT seq.i\n" +
                    "  FROM generate_series(1, " + n + ") AS seq(i);");
        }
    }

    private void createXml() throws IOException, XMLStreamException, SQLException {
        XMLOutputFactory output = XMLOutputFactory.newInstance();
        XMLStreamWriter writer = output.createXMLStreamWriter(new FileWriter(new File("1.xml")));
        writer.writeStartDocument();

        writer.writeStartElement("entries");

        try (Connection connection = getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery("SELECT * FROM \"TEST\"")) {
                    while (resultSet.next()) {
                        writer.writeStartElement("entry");
                        writer.writeStartElement("field");
                        writer.writeCharacters(String.valueOf(resultSet.getInt("FIELD")));
                        writer.writeEndElement();
                        writer.writeEndElement();
                    }
                }
            }
            writer.writeEndElement();
            writer.flush();
        }
    }

    public static void transform() {
        try {
            Source xslt = new StreamSource(new File("elementToAttribute.xslt"));

            Transformer transformer = TransformerFactory.newInstance().newTransformer(xslt);

            Source text = new StreamSource(new File("1.xml"));

            transformer.transform(text, new StreamResult(new File("2.xml")));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    int parseAndSumm() throws ParserConfigurationException, SAXException, IOException {

        final AtomicInteger sum = new AtomicInteger();
        File inputFile = new File("2.xml");
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();

        DefaultHandler handler = new DefaultHandler() {
            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes)
                    throws SAXException {

                if (qName.equalsIgnoreCase("entry")) {
                    String fieldString = attributes.getValue("field");
                    sum.addAndGet(Integer.parseInt(fieldString));
                }
            }

        };

        saxParser.parse(inputFile, handler);

        return sum.get();
    }

    public String getJdbcurl() {
        return jdbcurl;
    }

    public void setJdbcurl(String jdbcurl) {
        this.jdbcurl = jdbcurl;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getN() {
        return n;
    }

    public void setN(Integer n) {
        this.n = n;
    }
}
